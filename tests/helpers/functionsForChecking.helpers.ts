import { expect } from 'chai';

export function checkStatusCode(response, statusCode: 200 | 201 | 204 | 400 | 401 | 403 | 404 | 405 | 409 | 500 ){
  expect(response.statusCode, `Status code should be ${statusCode}`).to.equal(statusCode);
}

export function checkResponseBodyMessage(response, message: string) {
  expect(response.body.message, `Message should be ${message}`).to.equal(message);
}

export function checkResponseTime(response, maxResponseTime: number) {
  expect(response.timings.phases.total, `Response time should be less than ${maxResponseTime}ms`).to.be.lessThan(maxResponseTime);
}
export function checkSchema(response, schema:object) {
  expect(response.body).to.be.jsonSchema(schema);
}

export function checkResponseBodyStatus(response, status: string) {
  expect(response.body.status, `Status should be ${status}`).to.be.equal(status);
}

export function checkArticlesAmount(response, articles: number) {
  expect(response.body.length, `Articles amount should be ${articles}`).to.be.equal(articles);
}

export function checkResponseBodyType(response, type: string) {
  expect(response.body.type, `Type should be ${type}`).to.be.equal(type);
}
