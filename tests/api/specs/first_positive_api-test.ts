import { UserController } from '../lib/controllers/user.controller';
import { PetController } from '../lib/controllers/pet.controller';
import { StoreController } from '../lib/controllers/store.controller';

import { checkStatusCode, checkResponseTime, checkSchema, checkResponseBodyMessage, checkArticlesAmount } from '../../helpers/functionsForChecking.helpers';
import { expect } from 'chai';

const users = new UserController();
const pets = new PetController();
const store = new StoreController();
const schemas = require('./data/schemas_testData.json');
let chai = require('chai');
chai.use(require('chai-json-schema'));

let id, firstName, petId, petStatus, orderId, orderInfo;

describe ('Positive tests', () => {
  it ('Create user', async() => {
    let userSettings = {
      username: global.appConfig.user.username,
      firstName: global.appConfig.user.firstName,
      lastName: global.appConfig.user.lastName,
      email: global.appConfig.user.email,
      password: global.appConfig.user.password,
      phone: global.appConfig.user.phone,
      userStatus: global.appConfig.user.userStatus
    }
    let response = await users.createUser(userSettings);
    checkStatusCode(response, 200);
    checkResponseTime(response, 5000);
    checkSchema(response, schemas.schema_createUser);
    id = response.body.message;
  }),
  it ('Get user by name', async() => {
    let response = await users.getUser(global.appConfig.user.username);
    checkStatusCode(response, 200);
    checkResponseTime(response, 3000);
    checkSchema(response, schemas.schema_getUser);
  }),
  it ('Login user', async() => {
    let response = await users.loginUser(global.appConfig.user.username, global.appConfig.user.password);
    checkStatusCode(response, 200);
    checkResponseTime(response, 3000);
    checkSchema(response, schemas.schema_logUser);
  }),
  it ('Update user info', async() => {
    firstName = 'NewTestUserFirstName';
    let userSettings = {
      id: id,
      username: global.appConfig.user.username,
      firstName: 'NewTestUserFirstName',
      lastName: global.appConfig.user.lastName,
      email: global.appConfig.user.email,
      password: global.appConfig.user.password,
      phone: global.appConfig.user.phone,
      userStatus: global.appConfig.user.userStatus
    }
    let response = await users.updateUser(global.appConfig.user.username, userSettings);
    checkStatusCode(response, 200);
    checkResponseTime(response, 3000);
    checkSchema(response, schemas.schema_updateUser);
    checkResponseBodyMessage(response, id);
  }),
  it ('Logout user', async() => {
    let response = await users.logoutUser();
    checkStatusCode(response, 200);
    checkResponseTime(response, 3000);
    checkSchema(response, schemas.schema_logoutUser);
    checkResponseBodyMessage(response, 'ok');
  }),
  it ('Delete user', async() => {
    let response = await users.deleteUser(global.appConfig.user.username);
    checkStatusCode(response, 200);
    checkResponseTime(response, 3000);
    checkSchema(response, schemas.schema_deleteUser);
    checkResponseBodyMessage(response, global.appConfig.user.username);
  }),
  it ('Create pet', async() => {
    let petSettings = {
      id:22,
      category: {
        id: 1,
        name: 'dogs'
      },
      name: 'Rex',
      photoUrls: [
        'https://upload.wikimedia.org/wikipedia/commons/4/43/Cute_dog.jpg'
      ],
      tags: [
        {
          id: 2,
          name: 'puppy'
        }
      ],
      status: 'available',
    }
    let response = await pets.addPet(petSettings);
    petId = response.body.id;
    checkStatusCode(response, 200);
    checkResponseTime(response, 3000);
    checkSchema(response, schemas.schema_addPet);
  }),
  it ('Find pet by ID', async() => {
    let response = await pets.findPetById(petId);
    checkStatusCode(response, 200);
    checkResponseTime(response, 3000);
    checkSchema(response, schemas.schema_addPet);
  })
  it ('Update pet info', async() => {
    let petSettings = {
      id: 122,
      category: {
        id: 1,
        name: 'dogs'
      },
      name: 'Rex',
      photoUrls: [
        'https://upload.wikimedia.org/wikipedia/commons/4/43/Cute_dog.jpg'
      ],
      tags: [
        {
          id: 2,
          name: 'puppy'
        }
      ],
      status: 'pending',
    }
    let response = await pets.updatePetInfo(petSettings);
    petStatus = response.body.status;
    expect(petStatus).to.be.equal(petSettings.status);
    checkStatusCode(response, 200);
    checkResponseTime(response, 3000);
    checkSchema(response, schemas.schema_addPet);
  })
  it ('Find pet by Status', async() => {
    let response = await pets.findPetByStatus('pending');
    checkStatusCode(response, 200);
    checkResponseTime(response, 3000);
  })
  it('Place an order for a pet', async () => {
    let orderSettings = {
      id: 110,
      petId: petId,
      quantity: 1,
      shipDate: "2022-09-28T08:12:01.000+0000",
      status: "placed",
      complete: true
    }
    let response = await store.orderPet(orderSettings);
    orderId = response.body.id;
    orderInfo = response.body;
    checkStatusCode(response, 200);
    checkResponseTime(response, 3000);
    checkSchema(response, schemas.schema_orderPet);
  }),
  it('Find order by id', async () => {
    let response = await store.getOrderById(orderId);
    checkStatusCode(response, 200);
    checkResponseTime(response, 3000);
    checkSchema(response, schemas.schema_orderPet);
  }),
  it('Get store inventory', async () => {
    let response = await store.getStoreInventory();
    checkStatusCode(response, 200);
    checkResponseTime(response, 3000);
  }),
  it('Delete order', async () => {
    let response = await store.deleteOrder(orderId);
    checkStatusCode(response, 200);
    checkResponseTime(response, 3000);
    checkSchema(response, schemas.schema_deleteOrder);
    checkResponseBodyMessage(response, orderId.toString());
  })
})