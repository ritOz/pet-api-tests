import { UserController } from '../lib/controllers/user.controller';
import { PetController } from '../lib/controllers/pet.controller';
import { StoreController } from '../lib/controllers/store.controller';

import { checkStatusCode, checkResponseBodyType, checkResponseTime, checkSchema, checkResponseBodyMessage, checkArticlesAmount } from '../../helpers/functionsForChecking.helpers';
import { expect } from 'chai';

const users = new UserController();
const pets = new PetController();
const store = new StoreController();
const schemas = require('./data/schemas_testData.json');
let chai = require('chai');
chai.use(require('chai-json-schema'));

describe('Negative tests', () => {
  it('Register without username', async () => {
    let userSettings = {
      firstName: global.appConfig.user.firstName,
      lastName: global.appConfig.user.lastName,
      email: global.appConfig.user.email,
      password: global.appConfig.user.password,
      phone: global.appConfig.user.phone,
      userStatus: global.appConfig.user.userStatus
    }
    let response = await users.createUser(userSettings);
    checkStatusCode(response, 200);
    checkResponseTime(response, 5000);
    checkResponseBodyMessage(response, '0');
    checkSchema(response, schemas.schema_invalidRegistration);
  }),
  it('Get user by empty username', async () => {
    let response = await users.getUser(' ');
    checkStatusCode(response, 405);
    checkResponseTime(response, 3000);
    checkResponseBodyType(response, 'unknown');
    checkSchema(response, schemas.schema_emptyField);
  }),
  it('Get user by username with error', async () => {
    let response = await users.getUser(global.appConfig.user.username + 2);
    checkStatusCode(response, 404);
    checkResponseTime(response, 3000);
    checkResponseBodyType(response, 'error');
    checkResponseBodyMessage(response, 'User not found');
    checkSchema(response, schemas.schema_errorVal);
  }),
  it('Delete user with empty username', async () => {
    let response = await users.deleteUser(' ');
    checkStatusCode(response, 405);
    checkResponseTime(response, 3000);
    checkResponseBodyType(response, 'unknown');
    checkSchema(response, schemas.schema_emptyField);
  }),
  it('Delete user with invalid username', async () => {
    let response = await users.deleteUser(global.appConfig.user.username + 2);
    checkStatusCode(response, 404);
    checkResponseTime(response, 3000);
  }),
  it('Find pet by empty Id', async () => {
    let response = await pets.findPetById(0);
    checkStatusCode(response, 404);
    checkResponseBodyType(response, 'error');
    checkResponseTime(response, 3000);
    checkResponseBodyMessage(response, 'Pet not found');
    checkSchema(response, schemas.schema_errorVal);
  }),
  it('Find order by invalid Id', async () => {
    let response = await store.getOrderById(0);
    checkStatusCode(response, 404);
    checkResponseBodyType(response, 'error');
    checkResponseTime(response, 3000);
    checkResponseBodyMessage(response, 'Order not found');
    checkSchema(response, schemas.schema_errorVal);
  }),
  it('Delete order', async () => {
    let response = await store.deleteOrder(0);
    checkStatusCode(response, 404);
    checkResponseBodyType(response, 'unknown');
    checkResponseTime(response, 3000);
    checkResponseBodyMessage(response, 'Order Not Found');
    checkSchema(response, schemas.schema_errorVal);
    })
})