import { ApiRequest } from "../request";
let baseUrl: string = global.appConfig.baseUrl;

export class PetController {
  async addPet(petSettings: object) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("POST")
      .url(`pet`)
      .body(petSettings)
      .send();
    return response;
  }
  async findPetById(petId:number) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`pet/${petId}`)
      .send();
    return response;
  }
  async findPetByStatus(petStatus: string) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`pet/findByStatus?status=${petStatus}`)
      .send();
    return response;
  }
  async updatePetInfo(petSettings: object) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("PUT")
      .url(`pet`)
      .body(petSettings)
      .send();
    return response;
  }
}