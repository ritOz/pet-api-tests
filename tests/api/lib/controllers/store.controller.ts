import { ApiRequest } from "../request";
let baseUrl: string = global.appConfig.baseUrl;

export class StoreController {
  async orderPet(orderSettings: object) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("POST")
      .url(`store/order`)
      .body(orderSettings)
      .send();
    return response;
  }
  async getOrderById(orderId: number) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`store/order/${orderId}`)
      .send();
    return response;
  }
  async getStoreInventory() {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`store/inventory`)
      .send();
    return response;
  }
  async deleteOrder(orderId: number) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("DELETE")
      .url(`store/order/${orderId}`)
      .send();
    return response;
  }
}