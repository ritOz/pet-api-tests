import { ApiRequest } from "../request";
let baseUrl: string = global.appConfig.baseUrl;

export class UserController {
  async createUser(userSettings: object) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("POST")
      .url(`user`)
      .body(userSettings)
      .send();
    return response;
  }
  async getUser(userNameVal: string) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`user/${userNameVal}`)
      .send();
    return response;
  }
  async loginUser(userNameVal: string, passwordVal: string) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`user/login?username=${userNameVal}&password=${passwordVal}`)
      .send();
    return response;
  }
  async updateUser(userNameVal: string, userSettings: object) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("PUT")
      .url(`user/${userNameVal}`)
      .body(userSettings)
      .send();
    return response;
  }
  async deleteUser(userNameVal: string) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("DELETE")
      .url(`user/${userNameVal}`)
      .send();
    return response;
  }
  async logoutUser() {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`user/logout`)
      .send();
    return response;
  }
}