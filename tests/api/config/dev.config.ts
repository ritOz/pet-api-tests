global.appConfig = {
  envName: 'DEV Environment',
  baseUrl: 'https://petstore.swagger.io/v2/',

  user: {
    id: 100,
    username: 'TestUser',
    firstName: 'TestUserFirstName',
    lastName: 'TestUserLastName',
    email: 'test.email@gmail.com',
    password: 'test123',
    phone: '',
    userStatus: 0
  }
};
